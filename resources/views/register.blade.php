<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>w-3 d-2</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="{{ url('welcome') }}" method="POST">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        
        <label for="fname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>
        
        <label for="Gender">Gender:</label><br><br>
        <input type="radio" id="male"  value="Male">
            <label for="male">Male</label><br>
        <input type="radio" id="female"  value="Female">
            <label for="female">Female</label><br>
        <input type="radio" id="other" value="Other">
            <label for="other">Other</label> <br><br>
        
        <label for="nationality">Nationality:</label><br><br>
        <select id="nationality" name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="australia">Austalia</option>
        </select><br><br>

        <label for="Language">Language Spoken:</label><br><br>
        <input type="checkbox" value="Bahasa Indonesia">
            <label for="Bahasa_Indonesia"> Bahasa Indonesia</label><br>
        <input type="checkbox" value="English">
            <label for="English"> English</label><br>
        <input type="checkbox" value="Other">
            <label for="Other"> Other</label><br><br>
        
        <label for="bio">Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>