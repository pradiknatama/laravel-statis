@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Cast Detail</h1>
            </div>

          </div>
        </div><!-- /.container-fluid -->
    </section>
  
      <!-- Main content -->
      <section class="content">
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Cast Detail</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body ">
            <div class="post">
                <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="/admin/dist/img/user1-128x128.jpg" alt="user image">
                    <span class="username">
                      <a href="#">{{ $cast->nama }}</a>
                    </span>
                    <span class="description">{{ $cast->umur }} tahun</span>
                </div>
                <!-- /.user-block -->
                <p>
                    {{ $cast->bio }}
                </p>
              </div>
              <a href="/cast" class="btn btn-secondary">Kembali</a>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
  
      </section>
      <!-- /.content -->
@endsection