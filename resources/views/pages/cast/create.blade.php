@extends('layouts.master')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tambah Cast</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>  
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Tambah Cast</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="/cast" method="post">
              @csrf
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                  </div>
                  @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror

                  <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur">
                  </div>
                  @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="exampleInputEmail1">Bio</label>
                    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                  @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/cast" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection