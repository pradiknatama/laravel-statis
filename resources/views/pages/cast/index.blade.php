@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Daftar Pemain</h1>
            </div>

          </div>
        </div><!-- /.container-fluid -->
    </section>
  
      <!-- Main content -->
      <section class="content">
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Daftar Pemain</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body p-0">
              <a href="/cast/create" class="btn btn-primary m-3">Tambah Cast</a>
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th style="width: 1%">
                            #
                        </th>
                        <th style="width: 20%">
                            Nama
                        </th>
                        <th style="width: 15%">
                            Umur
                        </th>
                        <th>
                            Bio
                        </th>
                        <th style="width: 25%">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=>$item)
                    <tr>
                        
                        <td>
                            {{ $key+1 }}
                        </td>
                        <td>
                            {{ $item->nama }}
                        </td>
                        <td>
                            {{ $item->umur }} tahun
                        </td>
                        <td>
                            {{ $item->bio }} 
                        </td>
                        <td class="project-actions text-right">
                            <form action="/cast/{{ $item->id }}" method="post">
                                @csrf
                                @method('delete')
                                <a class="btn btn-primary btn-sm" href="/cast/{{ $item->id }}">
                                    <i class="fas fa-folder">
                                    </i>
                                    View
                                </a>
                                <a class="btn btn-info btn-sm" href="/cast/{{ $item->id }}/edit">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash">
                                </i>
                                Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                        Data Cast Masih Kosong
                    @endforelse

                </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
  
      </section>
      <!-- /.content -->
@endsection