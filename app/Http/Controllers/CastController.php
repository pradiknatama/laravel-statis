<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CastController extends Controller
{
    public function index(){
        $cast= DB::table('cast')->get();
        return view('pages.cast.index', compact('cast'));
    }

    public function create(){
        return view('pages.cast.create');
    }

    public function store(Request $request){
        $request->validate(
            [
                'nama'=>'required',
                'umur'=>'required',
                'bio'=>'required'
            ],
            [
                'nama.required'=>'Inputan Nama harus diisi',
                'umur.required'=>'Inputan Umur harus diisi',
                'bio.required'=>'Inputan Bio harus diisi',
            ]
        );
        DB::table('cast')->insert(
            [
                'nama'=>$request['nama'],
                'umur'=>$request['umur'],
                'bio'=>$request['bio']
            ]
        );
        return redirect('/cast');
    }
    public function show($id)
    {
        $cast= DB::table('cast')->where('id',$id)->first();
        return view('pages.cast.show',compact('cast'));
    }

    public function edit($id)
    {
        $cast= DB::table('cast')->where('id',$id)->first();
        return view('pages.cast.edit',compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama'=>'required',
                'umur'=>'required',
                'bio'=>'required'
            ],
            [
                'nama.required'=>'Inputan Nama harus diisi',
                'umur.required'=>'Inputan Umur harus diisi',
                'bio.required'=>'Inputan Bio harus diisi',
            ]
        );
        DB::table('cast')
            ->where('id',$id)
            ->update(
                [
                  'nama'=>$request['nama'],
                  'umur'=>$request['umur'],
                  'bio'=>$request['bio'] 
                ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
       DB::table('cast')->where('id',$id)->delete();
       return redirect('/cast');
    }
}
